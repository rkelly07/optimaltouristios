//
//  CitySelectorViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/10/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import UIKit
import CoreLocation
import Parse

class CitySelectorViewController: UIViewController {
    
    
    
    
    @IBOutlet weak var cityTable: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    let bostonpic = UIImage(named: "boston.jpg")
    let nycpic = UIImage(named: "nyc.jpg")
    let singaporepic = UIImage(named: "singapore.jpg")
    let parispic = UIImage(named: "paris.jpg")
    let milanpic = UIImage(named: "milan.jpg")
    var currentCity:String!
    var cityNumber:Int!
    var user:PFUser?
    
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    
    var citylist: [UIImage] = []
    var citynames = ["Boston", "New York City", "Singapore", "Milan", "Paris"]
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        /*let defaults = NSUserDefaults.standardUserDefaults()
        if (defaults.boolForKey("loggedIn") == false){
            let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
            let loginVC = loginStoryboard.instantiateViewControllerWithIdentifier("login") 
            self.presentViewController(loginVC, animated: true, completion: nil)
        }*/
        self.user = PFUser.currentUser()
        if self.user == nil {
            let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
            let logInVC = loginStoryboard.instantiateViewControllerWithIdentifier("login")
            presentViewController(logInVC, animated: true, completion: nil)
        cityTable.reloadData()
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        /*let manager = CLLocationManager()
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            manager.requestWhenInUseAuthorization()
        }*/
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.citylist.append(bostonpic!)
        self.citylist.append(nycpic!)
        self.citylist.append(singaporepic!)
        self.citylist.append(milanpic!)
        //self.citylist.append(parispic!)
        
        
        self.cityTable.rowHeight = screenSize.width/2
        
        cityTable.reloadData()
        
    }
    
    @IBAction func createButton(sender: AnyObject) {
        self.currentCity = ""
        self.cityNumber = 0
        performSegueWithIdentifier("create", sender:nil)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.citylist.count
    }
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell") as UITableViewCell!
        
        //Setup Label
        let citylabel = cell.contentView.viewWithTag(1) as! UILabel
        citylabel.text = citynames[indexPath.row]
        
        //Setup Image in each cell
        let img = UIImageView(frame: CGRectMake(0,0,self.screenSize.width,self.cityTable.rowHeight))
        img.contentMode = .ScaleToFill
        img.image = self.citylist[indexPath.row]
        cell.addSubview(img)
        cell.sendSubviewToBack(img)
        return cell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // When cell is clicked, set correct variable values and transistion to next view
        self.currentCity = self.citynames[indexPath.row]
        self.cityNumber = (indexPath.row)+1
        performSegueWithIdentifier("create", sender:nil)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Send variable information to next class
        if segue.identifier == "create" {
            let destination = segue.destinationViewController as! PreferencesViewController
            destination.incomingString = self.currentCity
            destination.incomingInt = self.cityNumber
        }
    }
    
}

