//
//  ConversationViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 2/25/16.
//  Copyright © 2016 Ryan Kelly. All rights reserved.
//

import Foundation
import Parse

class ConversationViewController: UIViewController {
    
    @IBOutlet weak var messagesTable: UITableView!
    @IBOutlet weak var usernameLabel: UINavigationItem!
    @IBOutlet weak var messageText: UITextField!
    
    var conversation:PFObject!
    var messages: [PFObject] = []
    var user: PFUser?
    var otherUser: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = PFUser.currentUser()
        if conversation["user1"] as! String == self.user!.username! {
            self.otherUser = conversation["user2"] as! String
        } else {
            self.otherUser = conversation["user1"] as! String
        }
        self.usernameLabel.title = otherUser
        self.loadData()
    }
    
    func loadData() {

        let id = self.conversation.objectId! as String
        
        let query = PFQuery(className: "Message")
        query.whereKey("conversation", equalTo: id)
        query.findObjectsInBackgroundWithBlock { (objects: [PFObject]?, error: NSError?) -> Void in
            if error != nil {
                NSLog(error!.description)
            } else {
                var unsortedMessages: [PFObject] = []
                
                for object in objects!{
                    unsortedMessages.append(object)
                }
                // sort conversations in increasing time order
                self.messages = unsortedMessages.sort( { ($0.updatedAt!).compare($1.updatedAt!) == NSComparisonResult.OrderedAscending } )
                self.messagesTable.reloadData()
            }
        }

    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.messages.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell") as UITableViewCell!
        let message = self.messages[indexPath.row]
        let myMessage = cell.contentView.viewWithTag(2) as! UILabel
        let theirMessage = cell.contentView.viewWithTag(1) as! UILabel
        if message["from"] as! String == self.user!.username! {
            myMessage.text = message["messageText"] as? String
            theirMessage.hidden = true
        } else {
            theirMessage.text = message["messageText"] as? String
            myMessage.hidden = true
        }
        return cell
    }
    
    @IBAction func sendButton(sender: AnyObject) {
        if self.messageText.text!.isEmpty {
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "You must type a message to send!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            self.view.endEditing(true)
            return
        }
        let newMessage = PFObject(className: "Message")
        
        newMessage["conversation"] = self.conversation.objectId!
        newMessage["from"] = self.user!.username!
        newMessage["to"] = self.otherUser
        newMessage["messageText"] = self.messageText.text
        
        newMessage.saveInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
            if error == nil {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "Message Sent", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                self.messageText.text = ""
                self.loadData()
                return
            }
        }
        self.view.endEditing(true)

        
    }
    
}