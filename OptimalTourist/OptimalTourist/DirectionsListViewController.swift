//
//  DirectionsListViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/30/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit

class DirectionsListViewController: UIViewController {
    

    
    @IBOutlet weak var directionsTable: UITableView!
    var incomingInstructions: [String!]!
    
    override func viewDidLoad(){
        super.viewDidLoad()
      
    }
    @IBAction func doneButton(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.incomingInstructions!.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell") as UITableViewCell!
        //Setup Label with dynamic cell sizing and label lines
        let stepLabel = cell.contentView.viewWithTag(1) as! UILabel
        stepLabel.text = "\(indexPath.row+1). \(incomingInstructions![indexPath.row])"
        directionsTable.estimatedRowHeight = 44.0
        directionsTable.rowHeight = UITableViewAutomaticDimension
        return cell
    }

}