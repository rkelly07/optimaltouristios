//
//  ItineraryListViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/25/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit

class ItineraryListViewController: UIViewController{
    
    //let defaults = NSUserDefaults.standardUserDefaults()
    var trip = NSUserDefaults.standardUserDefaults().objectForKey("trip") as! [[String:AnyObject]]
    var itinerary: [String]!
    var city: String!
    var currentStep = 0
    var currentTrip = NSUserDefaults.standardUserDefaults().integerForKey("currentTrip")
    
    @IBOutlet weak var itineraryTable: UITableView!
    
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        itinerary = trip[currentTrip]["itinerary"] as! [String]
        itinerary.insert("Overview", atIndex: 0)
        city = trip[currentTrip]["city"] as! String
        //itinerary = defaults.objectForKey("itinerary") as! [String!]!
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.itinerary!.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell") as UITableViewCell!
        //Setup Label with dynamic cell sizing and label lines
        let stepLabel = cell.contentView.viewWithTag(1) as! UILabel
        stepLabel.text = itinerary![indexPath.row]
        itineraryTable.estimatedRowHeight = 44.0
        itineraryTable.rowHeight = UITableViewAutomaticDimension
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // When cell is clicked, set correct variable values and transistion to next view
        self.currentStep = indexPath.row
        performSegueWithIdentifier("poiPress", sender:nil)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Send variable information to next class
        if segue.identifier == "poiPress" {
            let destination = segue.destinationViewController as! MapViewController
            destination.incomingStep = self.currentStep
        }
    }
}