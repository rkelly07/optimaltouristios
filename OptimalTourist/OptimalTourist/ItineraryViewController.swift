//
//  ItineraryViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/15/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit
var switches:[[[Bool]]] = [[],[],[]]
var currentItinerary = 0


class ItineraryViewController: UIViewController{
    
    
    @IBOutlet weak var itineraryTable: UITableView!
    @IBOutlet weak var itinerarySelector: UISegmentedControl!
    
    var incomingCity: String!
    var incomingItinerary: [[String!]]!
    var incomingLocations: [[String!]]!
    var incomingTimes: [[Double]]!
    var timeItinerary: [[String!]]!
    var trip: [[String:AnyObject]!] = [Dictionary<String, AnyObject>(),Dictionary<String, AnyObject>(),Dictionary<String, AnyObject>()]
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(false)
        
    }
    override func viewDidLoad(){
        super.viewDidLoad()
        self.timeItinerary = self.incomingItinerary
        for x in 0...2 {
            for _ in 0...(self.incomingTimes[x].count-1){
                switches[x].append([])
            }

            for z in 0...(switches[x].count-1){
                for y in 0...3{
                    if y == 0{
                        switches[x][z].append(true)
                    } else {
                        switches[x][z].append(false)
                    }
                }
            }
        }

                for j in 0...2 {
                    add_time_to_itinerary(j)
                }
        
        
    }
    
    func add_time_to_itinerary(functionCall: Int) {
        for x in 0...(self.timeItinerary[functionCall].count-1) {
            self.timeItinerary[functionCall][x] = " \(x+1). \(self.incomingItinerary[functionCall][x])"
        }
        let progress = ProgressViewController()
        for x in 0...(incomingItinerary[functionCall].count-1) {
            self.timeItinerary[functionCall][x].insertContentsOf(" \(progress.formatTime(self.incomingTimes[functionCall][x]))".characters, at: self.timeItinerary[functionCall][x].endIndex.predecessor())
        }
        self.itineraryTable.reloadData()
    }
    @IBOutlet weak var homeButton: UIBarButtonItem!
    @IBAction func menuButton(sender: AnyObject) {
        let storyboard1 = UIStoryboard(name: "Planner", bundle: nil)
        let vc1 = storyboard1.instantiateViewControllerWithIdentifier("city") 
        presentViewController(vc1, animated: true, completion: nil)
    }
    @IBAction func changeItinerary(sender: AnyObject) {
        currentItinerary = itinerarySelector.selectedSegmentIndex
        self.itineraryTable.reloadData()

    }



    @IBAction func updateButton(sender: AnyObject) {
        var changeArrays: [[Int]] = [[], [], [], []]
        var allowance: Double = 0
        
        //Validates that every row has at least one switch in the on position
        var switchCheck = true
        for switcharr in switches[currentItinerary]{
            var switchTest = false
            for sw in switcharr{
                if sw {
                    switchTest = true
                    break
                }
            }
            if switchTest == false {
                switchCheck = false
                break
            }
        }
        if switchCheck == false{
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "You must have something selected for each row!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        //Validates that if a plus is switched there are other items that are being decreased or removed
        var equal = false
        var plus = false
        var minus = false
        var delete = false
        for switcharr in switches[currentItinerary]{
            if switcharr[0] {
                equal = true
            }
            if switcharr[1] {
                plus = true
            }
            if switcharr[2] {
                minus = true
            }
            if switcharr[3] {
                delete = true
            }
        }
        if plus == true{
            if minus == false && delete == false {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "You must select what item you wish to remove time from!", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                return
            }
        }
        if minus == true || delete == true{
            if plus == false{
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "You must select an item you wish to add time to!", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                return
            }
        }

        
        for x in 0...(switches[currentItinerary].count-1){
            let currentArray = switches[currentItinerary][x]
            for y in 0...(currentArray.count-1){
                if currentArray[y] == true{
                    changeArrays[y].append(x)
                }
            }
        }
        for minus in changeArrays[2]{
            self.incomingTimes[currentItinerary][minus] = self.incomingTimes[currentItinerary][minus]/2
            allowance += self.incomingTimes[currentItinerary][minus]
        }
        if delete == true {
            for delete in changeArrays[3]{
                allowance += self.incomingTimes[currentItinerary][delete]
            }
            if changeArrays[1].count != 0 {
                let timetoadd = allowance/Double(changeArrays[1].count)
                for add in changeArrays[1]{
                    self.incomingTimes[currentItinerary][add] += timetoadd
                }
            }
            removeFromSwitches(changeArrays[3])
            removeFromIncomingTimes(changeArrays[3])
            removeFromIncomingItinerary(changeArrays[3])
            self.timeItinerary[currentItinerary] = self.incomingItinerary[currentItinerary]
        }
        else {
            if changeArrays[1].count != 0 {
                let timetoadd = allowance/Double(changeArrays[1].count)
                for add in changeArrays[1]{
                    self.incomingTimes[currentItinerary][add] += timetoadd
                }
            }

        }
        self.add_time_to_itinerary(currentItinerary)
        self.itineraryTable.reloadData()
    }
    func removeFromIncomingTimes(delete: [Int]) {
        var timedict = [Int: Double]()
        for x in 0...(self.incomingTimes[currentItinerary].count-1){
            timedict[x] = self.incomingTimes[currentItinerary][x]
        }
        var sortedTimes = [(Int,Double)]()
        var newTimes: [Double]! = []
        for (key, val) in timedict {
            sortedTimes.append((key, val))
        }
        sortedTimes.sortInPlace({$0.0 < $1.0})
        for pair in sortedTimes {
            if delete.contains(pair.0){
                continue
            }
            newTimes.append(pair.1)
        }
        self.incomingTimes[currentItinerary] = newTimes
    }
    
    
    func removeFromIncomingItinerary(delete: [Int]){
        var itindict = [Int: String!]()
        for x in 0...(self.incomingItinerary[currentItinerary].count-1){
            itindict[x] = self.incomingItinerary[currentItinerary][x]
        }
        var sortedItin = [(Int,String!)]()
        var newitin: [String!]! = []
        for (key, val) in itindict {
            sortedItin.append((key, val))
        }
        sortedItin.sortInPlace({$0.0 < $1.0})
        for pair in sortedItin {
            if delete.contains(pair.0){
                continue
            }
            newitin.append(pair.1)
        }
        self.incomingItinerary[currentItinerary] = newitin
    }
    func removeFromSwitches(delete: [Int]){
        var switchdict = [Int: [Bool]]()
        for x in 0...(switches[currentItinerary].count-1){
            switchdict[x] = switches[currentItinerary][x]
        }
        var sortedswitch = [(Int,[Bool])]()
        var newswitch: [[Bool]] = []
        for (key, val) in switchdict {
            sortedswitch.append((key, val))
        }
        sortedswitch.sortInPlace({$0.0 < $1.0})
        for pair in sortedswitch {
            if delete.contains(pair.0){
                continue
            }
            newswitch.append(pair.1)
        }
        switches[currentItinerary] = newswitch
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.timeItinerary[currentItinerary].count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell")! as UITableViewCell
        //Setup Label with dynamic cell sizing and label lines
        let stepLabel = cell.contentView.viewWithTag(1) as! UILabel
        
        let switcharray = [cell.contentView.viewWithTag(2) as! customSwitch, cell.contentView.viewWithTag(3) as! customSwitch, cell.contentView.viewWithTag(4) as! customSwitch, cell.contentView.viewWithTag(5) as! customSwitch]
        for x in 0...(switcharray.count-1){
            switcharray[x].setOn(switches[currentItinerary][indexPath.row][x] ,animated: false)
            switcharray[x].row = indexPath.row
        }
        stepLabel.text = timeItinerary[currentItinerary][indexPath.row]
        itineraryTable.estimatedRowHeight = 88.0
        itineraryTable.rowHeight = UITableViewAutomaticDimension
        cell.selectionStyle = .None
        return cell
    }
    @IBAction func mapsButton(sender: AnyObject) {
        for x in 0...2 {
            trip[x]["itinerary"] = self.timeItinerary[x]
            trip[x]["locations"] = self.incomingLocations[x]
            trip[x]["city"] = self.incomingCity
        }
        defaults.setInteger(0, forKey: "currentTrip")
        defaults.setObject(self.trip, forKey: "trip")
        defaults.synchronize()
        let mapsStoryboard = UIStoryboard(name: "Maps", bundle: nil)
        let mapsVC = mapsStoryboard.instantiateViewControllerWithIdentifier("maps") 
        self.presentViewController(mapsVC, animated: true, completion: nil)
    }
    /*func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // When cell is clicked, set correct variable values and transistion to next view
        let alert = UIAlertController(title: "Alert", message: "\(timeItinerary[indexPath.row])", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            switch action.style{
            case .Default:
                let deletedTime = self.incomingTimes[indexPath.row]
                let timetoadd = deletedTime/Double(self.timeItinerary.count-1)
                self.incomingTimes.removeAtIndex(indexPath.row)
                self.incomingItinerary.removeAtIndex(indexPath.row)
                for x in 0...(self.incomingTimes.count-1) {
                    self.incomingTimes[x] += timetoadd
                }
                self.add_time_to_itinerary()
                tableView.reloadData()
                
            case .Cancel:
                print("cancel")
                
            case .Destructive:
                print("destructive")
            }
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }*/
}
// Custom subclass for each cell in the row that allows each rows switches to be handled individually
class customCell: UITableViewCell {

    @IBOutlet weak var equalSwitch: customSwitch!
    @IBOutlet weak var plusSwitch: customSwitch!
    @IBOutlet weak var minusSwitch: customSwitch!
    @IBOutlet weak var deleteSwitch: customSwitch!
    
    //functions for switch that makes sure only one is active at a time
    @IBAction func equalSwitchChange(sender: AnyObject) {
        switches[currentItinerary][sender.row] = [!switches[currentItinerary][sender.row][0], false, false, false]
        plusSwitch.setOn(false, animated: true)
        minusSwitch.setOn(false, animated: true)
        deleteSwitch.setOn(false, animated: true)
    }
    @IBAction func plusSwitchChange(sender: AnyObject) {
        switches[currentItinerary][sender.row] = [false, !switches[currentItinerary][sender.row][1], false, false]
        equalSwitch.setOn(false, animated: true)
        minusSwitch.setOn(false, animated: true)
        deleteSwitch.setOn(false, animated: true)
    }
    
    @IBAction func minusSwitchChange(sender: AnyObject) {
        switches[currentItinerary][sender.row] = [false, false,!switches[currentItinerary][sender.row][2], false]
        equalSwitch.setOn(false, animated: true)
        plusSwitch.setOn(false, animated: true)
        deleteSwitch.setOn(false, animated: true)
    }
    
    @IBAction func deleteSwitchChange(sender: AnyObject) {
        switches[currentItinerary][sender.row] = [false, false, false, !switches[currentItinerary][sender.row][3]]
        print(sender.row)
        print(switches)
        equalSwitch.setOn(false, animated: true)
        plusSwitch.setOn(false, animated: true)
        minusSwitch.setOn(false, animated: true)
    }
}
class customSwitch: UISwitch {
    var row: Int = 0
}