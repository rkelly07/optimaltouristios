//
//  MapCheck.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 5/26/16.
//  Copyright © 2016 Ryan Kelly. All rights reserved.
//

import Foundation
import CoreLocation

class MapCheck {
    
    let manager: CLLocationManager
    
    init(){
        manager = CLLocationManager()
    }
    
    
    func checkMapPermissions() -> Bool{
        if CLLocationManager.authorizationStatus() != .AuthorizedAlways || CLLocationManager.authorizationStatus() != .AuthorizedWhenInUse {
            return false
        }
        return true
    }
    func requestMapPermissions() {
        manager.requestWhenInUseAuthorization()
    }
}