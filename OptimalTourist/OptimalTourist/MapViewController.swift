//
//  MapViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/25/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import MapKit
import AddressBook
import CoreLocation
import UIKit
import Parse

class MapViewController: UIViewController, MKMapViewDelegate, SWRevealViewControllerDelegate, CLLocationManagerDelegate, FriendDisplayDelegate {
    
    @IBOutlet weak var itineraryListButton: UIBarButtonItem!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var destinationLabel: UILabel!
    
    var locationManager: CLLocationManager!
    let defaults = NSUserDefaults.standardUserDefaults()
    let span: MKCoordinateSpan = MKCoordinateSpanMake(0.05,0.05)
    
    let bostonCoords: CLLocationCoordinate2D = CLLocationCoordinate2DMake(42.3601, -71.0589)
    var bostonRegion: MKCoordinateRegion!
    
    var nycCoords: CLLocationCoordinate2D = CLLocationCoordinate2DMake(40.7127, -74.0059)
    var nycRegion: MKCoordinateRegion!
    var nycPlacemark: CLPlacemark!
    
    let singaporeCoords: CLLocationCoordinate2D = CLLocationCoordinate2DMake(1.282375, 103.864273)
    var singaporeRegion: MKCoordinateRegion!
    var singaporePlacemark: CLPlacemark!
    
    let parisCoords: CLLocationCoordinate2D = CLLocationCoordinate2DMake(48.8567, 2.3508)
    var parisRegion: MKCoordinateRegion!
    var parisPlacemark: CLPlacemark!
    
    let milanCoords: CLLocationCoordinate2D = CLLocationCoordinate2DMake(45.4667, 9.1833)
    var milanRegion: MKCoordinateRegion!
    var milanPlacemark: CLPlacemark!
    
    var region: MKCoordinateRegion!
    
    var sidebarMenuOpen: Bool!
    var currentLocations: [String!]!
    var destination: MKMapItem!
    var destination2: MKMapItem!
    var instructionsList: [String!]! = []
    var incomingStep: Int?
    var currentStep = 0
    var currentCity: String!
    
    var currentCoordinates:CLLocation?
    
    var user:PFUser?
    
    var currentLocation: MKMapItem!
    
    var trips = NSUserDefaults.standardUserDefaults().objectForKey("trip") as! [[String:AnyObject]]
    var currentTripNumber = NSUserDefaults.standardUserDefaults().integerForKey("currentTrip")
    var currentTrip: [String:AnyObject]!
    
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(false)
        
        
    }
    override func viewDidLoad() {
        /*if currentTrip == nil {
            self.performSegueWithIdentifier("tripSelect", sender: nil)
        }*/
        
        //var geoCoder = CLGeocoder()

        
        if (self.incomingStep != nil) {
            self.currentStep = self.incomingStep!
        }
        self.currentTrip = self.trips[currentTripNumber]
        self.currentCity = self.currentTrip["city"] as? String
        
        
        
        bostonRegion = MKCoordinateRegionMake(self.bostonCoords, self.span)
        singaporeRegion = MKCoordinateRegionMake(self.singaporeCoords, self.span)
        nycRegion = MKCoordinateRegionMake(self.nycCoords, self.span)
        parisRegion = MKCoordinateRegionMake(self.parisCoords, self.span)
        milanRegion = MKCoordinateRegionMake(self.milanCoords, self.span)
        
        
        
        sidebarMenuOpen = false
        self.revealViewController().delegate = self
        self.mapView.delegate = self
        currentLocations = self.currentTrip["locations"] as? [String!]!
        currentLocations.insert("Overview", atIndex: 0)
        destinationLabel.text = "Destination: \r\n \(self.currentLocations[self.currentStep])"
        
        
        
        if self.currentCity == "Boston" {
            self.region = self.bostonRegion
            self.currentLocation = MKMapItem.mapItemForCurrentLocation()
            
            if (CLLocationManager.locationServicesEnabled())
            {
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
                self.mapView.showsUserLocation = true
                self.mapView.centerCoordinate = self.mapView.userLocation.coordinate
                self.updateUserInfo()
                self.getdest("\(self.currentLocations[self.currentStep]) \(self.currentCity)")
            }
        } else if self.currentCity == "Milan" {
            self.region = self.milanRegion
            CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: 45.4667, longitude: 9.1833), completionHandler: { placemarks, error -> Void in
                if error != nil {
                    print("Geocode failed with error: \(error!.localizedDescription)")
                }
                
                if placemarks!.count > 0 {
                    self.milanPlacemark = placemarks![0]
                }
                self.currentLocation = MKMapItem(placemark: MKPlacemark(placemark: MKPlacemark(placemark: self.milanPlacemark)))
                self.updateUserInfo()
                self.getdest("\(self.currentLocations[self.currentStep]) \(self.currentCity)")
                
            })
            
        } else if self.currentCity == "New York City" {
            self.region = self.nycRegion
            self.currentLocation = MKMapItem.mapItemForCurrentLocation()
            
            if (CLLocationManager.locationServicesEnabled())
            {
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
                self.mapView.showsUserLocation = true
                self.mapView.centerCoordinate = self.mapView.userLocation.coordinate
                self.updateUserInfo()
                self.getdest("\(self.currentLocations[self.currentStep]) \(self.currentCity)")
            }
        } else if self.currentCity == "Singapore" {
            self.region = self.singaporeRegion

            CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: 1.282375, longitude: 103.864273), completionHandler: { placemarks, error -> Void in
                if error != nil {
                    print("Geocode failed with error: \(error!.localizedDescription)")
                }
                
                if placemarks!.count > 0 {
                    self.singaporePlacemark = placemarks![0]
                }
                self.currentLocation = MKMapItem(placemark: MKPlacemark(placemark: MKPlacemark(placemark: self.singaporePlacemark)))
                self.updateUserInfo()
                self.getdest("\(self.currentLocations[self.currentStep]) \(self.currentCity)")
            })
                
        } else if self.currentCity == "Paris" {
            self.region = self.parisRegion
            CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: 48.8567, longitude: 2.3508), completionHandler: { placemarks, error -> Void in
                if error != nil {
                    print("Geocode failed with error: \(error!.localizedDescription)")
                }
                
                if placemarks!.count > 0 {
                    self.parisPlacemark = placemarks![0]
                }
                self.currentLocation = MKMapItem(placemark: MKPlacemark(placemark: MKPlacemark(placemark: self.parisPlacemark)))
                self.updateUserInfo()
                self.getdest("\(self.currentLocations[self.currentStep]) \(self.currentCity)")
                
            })
            
            
        }
        
        
        self.mapView.setRegion(self.region, animated: true)
        
        if self.revealViewController() != nil {
            itineraryListButton.target = self.revealViewController()
            itineraryListButton.action = "rightRevealToggle:"
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        
        
        
        
        
        
        
    

    }
    func updateUserInfo() {
        self.user = PFUser.currentUser()
        self.user!["currentDestination"] = self.currentLocations[self.currentStep]
        self.user!["currentCity"] = self.currentCity
        
        self.user!["latitude"] = String(self.currentLocation.placemark.coordinate.latitude)
        self.user!["longitude"] = String(self.currentLocation.placemark.coordinate.longitude)
        
        
        self.user!.saveInBackgroundWithBlock{ (succ, error) -> Void in
            if error == nil {
                print("SUCCESS")
            }
        }
    }
    @IBAction func viewDirectionsButton(sender: AnyObject) {
        self.performSegueWithIdentifier("directions", sender: nil)
    }
    func showRoute(response: MKDirectionsResponse) {
        
        for route in response.routes {
            
            mapView.addOverlay(route.polyline,
                level: MKOverlayLevel.AboveRoads)
            for step in route.steps {
                self.instructionsList.append(step.instructions)
            }
        }
        
    }
    func showOverView(routes: [MKRoute]) {
        for route in routes {
    
            mapView.addOverlay(route.polyline,
                level: MKOverlayLevel.AboveRoads)
            for step in route.steps {
                self.instructionsList.append(step.instructions)
            }
        }
    }
    func mapView(mapView: MKMapView, rendererForOverlay
        overlay: MKOverlay) -> MKOverlayRenderer {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.blueColor()
            renderer.lineWidth = 5.0
            return renderer
    }
    @IBAction func changeTripButton(sender: AnyObject) {
        self.performSegueWithIdentifier("tripSelect", sender: nil)
    }
    
    
    func getdest(locationName: String){
        if locationName == "Overview \(self.currentCity)" {
            //var locationlist: [MKMapItem!] = []
            for i in 1...(self.currentLocations.count-2){
                let j = i + 1
                let localSearchRequest = MKLocalSearchRequest()
                localSearchRequest.naturalLanguageQuery = self.currentLocations[i]
                localSearchRequest.region = self.region
                if self.currentLocations[i] == "National Park Service Visitor Center Boston" {
                    localSearchRequest.naturalLanguageQuery = "Faneuil Hall, First Floor, Boston, MA 02109"
                }
                print(" Origin \(i) \(localSearchRequest.naturalLanguageQuery)")
                let localSearch = MKLocalSearch(request: localSearchRequest)
                localSearch.startWithCompletionHandler { (response, error) in
                    if error != nil {
                        let alertController = UIAlertController(title: "OptimalTourist", message:
                            "API request limit exceeded, please try again", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                        return
                    } else {
                    self.destination = response!.mapItems[0]
                    localSearch.cancel()
                    /*let currentAnnotation = MKPointAnnotation()
                    currentAnnotation.title = "Test User 1"
                    currentAnnotation.coordinate =*/
                    var newDestination = ColorPointAnnotation(pinColor: UIColor.redColor())
                    newDestination.title = self.currentLocations[i]
                    newDestination.coordinate = self.destination.placemark.coordinate
                    newDestination.subtitle = "POI"
                    self.mapView.addAnnotation(newDestination)
                    let localSearchRequest2 = MKLocalSearchRequest()
                    localSearchRequest2.naturalLanguageQuery = self.currentLocations[j]
                    localSearchRequest2.region = self.region
                    print("Destination \(j) \(localSearchRequest2.naturalLanguageQuery)")
                    let localSearch2 = MKLocalSearch(request: localSearchRequest)
                    localSearch2.startWithCompletionHandler { (response, error) in
                        localSearch.cancel()
                        if error != nil {
                            let alertController = UIAlertController(title: "OptimalTourist", message:
                                "API request limit exceeded, please try again", preferredStyle: UIAlertControllerStyle.Alert)
                            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                            self.presentViewController(alertController, animated: true, completion: nil)
                            return
                        }
                        self.destination2 = response!.mapItems[0]
                        let request = MKDirectionsRequest()
                        request.source = (self.destination)
                        request.destination = (self.destination2!)
                        request.requestsAlternateRoutes = false
                        request.transportType = MKDirectionsTransportType.Walking
                        
                        let directions = MKDirections(request: request)
                        
                        directions.calculateDirectionsWithCompletionHandler({response, error in
                            
                            if error != nil {
                                print(error!.localizedDescription)
                            } else {
                                self.showRoute(response!)
                            }
                            
                        })
                    }
                    }
                }
                }
    }

                /*let localSearchRequest = MKLocalSearchRequest()
                localSearchRequest.naturalLanguageQuery = self.currentTrip[i]
                print (self.currentTrip[i])
                localSearchRequest.region = self.region
                let localSearch = MKLocalSearch(request: localSearchRequest)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                localSearch.startWithCompletionHandler { (response, error) in
                    //locationlist.append(response!.mapItems[0])
                }
                })*/
                
                    //var responselist: [MKRoute] = []
                
               // dispatch_async(dispatch_get_main_queue(), { () -> Void in

                    /*for j in 0...(locationlist.count-2){
                        let request = MKDirectionsRequest()
                        request.source = (locationlist[j])
                        request.destination = (locationlist[j+1])
                        request.requestsAlternateRoutes = false
                        request.transportType = MKDirectionsTransportType.Walking
                        
                        let directions = MKDirections(request: request)
                                                directions.calculateDirectionsWithCompletionHandler({response, error in
                            
                            if error != nil {
                                print(error!.localizedDescription)
                            } else {
                                responselist += (response!.routes)
                            }
                        })
                        
                        
                    }
                    })
                    self.showOverView(responselist)
                }*/
        //}
        //}
        else {
            let localSearchRequest = MKLocalSearchRequest()
            print (locationName)
            var locName = locationName
            if locationName == "Torri Unicredit Milan"{
                locName = "Piazza Gae Aulenti, 20124 Milan, Italy"
            }
            localSearchRequest.naturalLanguageQuery = locName
            localSearchRequest.region = self.region

            let localSearch = MKLocalSearch(request: localSearchRequest)
            localSearch.startWithCompletionHandler { (response, error) in
                localSearch.cancel()
                if error != nil {
                    let alertController = UIAlertController(title: "OptimalTourist", message:
                        "API request limit exceeded. Please try again", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
                self.destination = response!.mapItems[0]
                var newDestination = ColorPointAnnotation(pinColor: UIColor.redColor())
                newDestination.title = locationName
                newDestination.coordinate = self.destination.placemark.coordinate
                newDestination.subtitle = "POI"
                self.mapView.addAnnotation(newDestination)
                let request = MKDirectionsRequest()
                request.source = (self.currentLocation)
                request.destination = (self.destination!)
                request.requestsAlternateRoutes = false
                request.transportType = MKDirectionsTransportType.Walking
                
                let directions = MKDirections(request: request)
            
                directions.calculateDirectionsWithCompletionHandler({response, error in
                
                    if error != nil {
                        print(error!.localizedDescription)
                    } else {
                        self.showRoute(response!)
                    }
                
                })
            }
        }
        
    }
    func revealController(revealController: SWRevealViewController!,  willMoveToPosition position: FrontViewPosition){
        if(position == FrontViewPosition.Left){
            self.mapView.zoomEnabled = true
            self.mapView.scrollEnabled = true
            sidebarMenuOpen = false
        } else {
            self.mapView.zoomEnabled = false
            self.mapView.scrollEnabled = false
            sidebarMenuOpen = true
        }
    }
    
    func revealController(revealController: SWRevealViewController!,  didMoveToPosition position: FrontViewPosition){
        if(position == FrontViewPosition.Left){
            self.mapView.zoomEnabled = true
            self.mapView.scrollEnabled = true
            sidebarMenuOpen = false
        } else {
            self.mapView.zoomEnabled = false
            self.mapView.scrollEnabled = false
            //self.mapView.userInteractionEnabled = false
            sidebarMenuOpen = true
        }
    }
    /*func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        let location = locations.last as! CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
    }*/
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Prepare to send the itinerary to the next class when the segue is performed
        if segue.identifier == "directions" {
            let destination = segue.destinationViewController as! DirectionsListViewController
            destination.incomingInstructions = self.instructionsList
        }
    }
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        print(view.canShowCallout)
        
    }
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView? {
        if let annotation = annotation {
            let identifier = "pin"
            var view: MKPinAnnotationView
            /*if let dequeuedView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
                as? MKPinAnnotationView { // 2
                    dequeuedView.annotation = annotation
                    let coloredPinAnnotation = annotation as! ColorPointAnnotation
                if #available(iOS 9.0, *) {
                    dequeuedView.pinTintColor = coloredPinAnnotation.pinColor
                    view = dequeuedView
                } else {
                    view = dequeuedView
                }
            } else {*/
                // 3
                
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                //let coloredPinAnnotation = annotation as! ColorPointAnnotation
                if #available(iOS 9.0, *) {
                    //view.pinTintColor = coloredPinAnnotation.pinColor
                    var test = annotation.title!! as String
                    if test == "Current Location" {
                        view.pinTintColor = UIColor.greenColor()
                    } else if annotation.subtitle!! as String == "friend"{
                        view.pinTintColor = UIColor.blueColor()
                    } else {
                        view.pinTintColor = UIColor.redColor()
                    }
                    view.canShowCallout = true
                    view.calloutOffset = CGPoint(x: -5, y: 5)
                    view.rightCalloutAccessoryView = UIButton(type: .DetailDisclosure) as UIView
                } else {
                    // Fallback on earlier versions
                //}
                
                
            }
            
            return view
        }
        return nil
    }
    @IBAction func showSocial(sender: AnyObject) {
        var socialVC = self.storyboard?.instantiateViewControllerWithIdentifier("social") as! SocialViewController
        socialVC.delegate = self
        self.presentViewController(socialVC, animated: true, completion: nil)
    }
    
    func sendBackFriendList(friendList: [ColorPointAnnotation]) {
        self.mapView.annotations.forEach {
            if $0.subtitle! != nil {
                if $0.subtitle!! == "friend" {
                    self.mapView.removeAnnotation($0)
                }
            }
            
        }
        for friend in friendList {
            self.mapView.addAnnotation(friend)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentCoordinates = locationManager.location!
        
    }
}

