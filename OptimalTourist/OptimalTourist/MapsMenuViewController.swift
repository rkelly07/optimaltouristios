//
//  MapsMenuViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/25/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation

class MapsMenuViewController: MenuViewController {
    
    /*override func viewWillAppear(animated: Bool){
        super.viewWillAppear(false)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
    }
    override func viewDidDisappear(animated: Bool){
        super.viewDidDisappear(false)
        self.revealViewController().frontViewController.view.userInteractionEnabled = true
        
    }*/
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0: // Trip Planner
            let storyboard1 = UIStoryboard(name: "Planner", bundle: nil)
            let vc1 = storyboard1.instantiateViewControllerWithIdentifier("city") 
            presentViewController(vc1, animated: true, completion: nil)
            break
        case 1: // Maps - current view
            self.performSegueWithIdentifier("mapsPress", sender: nil)
            break
        case 2: //Settings
            let storyboard3 = UIStoryboard(name: "Settings", bundle: nil)
            let vc3 = storyboard3.instantiateViewControllerWithIdentifier("settings")
            presentViewController(vc3, animated: true, completion: nil)
            break
        case 3: // Messaging
            let storyboard4 = UIStoryboard(name: "Messaging", bundle: nil)
            let vc4 = storyboard4.instantiateViewControllerWithIdentifier("messaging")
            presentViewController(vc4, animated: true, completion: nil)
            break
        default: // Error
            NSLog("error; invalid row tapped")
            break
        }
    }
    
}