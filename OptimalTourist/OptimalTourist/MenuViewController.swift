//
//  MenuViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/22/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit
import Parse

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var user:PFUser?
    
    struct MenuItem {
        let name:String!
    }
    
    let menuItems = [MenuItem(name: "Plan Trip"), MenuItem(name: "Maps"), MenuItem(name: "Settings"),MenuItem(name: "Message")]

    override func viewDidLoad() {
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath) 
        cell.textLabel?.text = menuItems[indexPath.row].name
        
        return cell
    }

    
    @IBAction func logOut(sender: AnyObject) {
        PFUser.logOut()
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        let loginVC = loginStoryboard.instantiateViewControllerWithIdentifier("login") 
        self.presentViewController(loginVC, animated: true, completion: nil)
    }
    func checkIfTour() -> Bool{
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if (defaults.objectForKey("trip") == nil) {
            return false
        }
        return true
    }
}