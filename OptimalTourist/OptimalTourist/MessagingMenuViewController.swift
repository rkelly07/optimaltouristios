//
//  MessagingMenuViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 2/24/16.
//  Copyright © 2016 Ryan Kelly. All rights reserved.
//

import Foundation

class MessagingMenuViewController: MenuViewController {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let mapcheck = MapCheck()
        switch indexPath.row {
        case 0: // Trip Planner - current view
            let storyboard1 = UIStoryboard(name: "Planner", bundle: nil)
            let vc1 = storyboard1.instantiateViewControllerWithIdentifier("city")
            presentViewController(vc1, animated: true, completion: nil)
            break
        case 1: // Maps
            
            if checkIfTour(){
                let storyboard2 = UIStoryboard(name: "Maps", bundle: nil)
                let vc2 = storyboard2.instantiateViewControllerWithIdentifier("maps")
                presentViewController(vc2, animated: true, completion: nil)
                break
            } else {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "You must plan a trip first or Turn on Location Settings!", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                break
            }
        case 2: //Settings
            let storyboard3 = UIStoryboard(name: "Settings", bundle: nil)
            let vc3 = storyboard3.instantiateViewControllerWithIdentifier("settings")
            presentViewController(vc3, animated: true, completion: nil)
            break
        case 3: //Messaging
            self.performSegueWithIdentifier("messagingPress", sender: nil)
            break
            
        default: // Error
            NSLog("error; invalid row tapped")
            break
        }
    }
}