//
//  MessagingViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 2/24/16.
//  Copyright © 2016 Ryan Kelly. All rights reserved.
//

import Foundation
import Parse

class MessagingViewController: UIViewController {
    
    @IBOutlet weak var conversationTable: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    
    var conversations: [PFObject] = []
    var currentConversation:PFObject?
    var user: PFUser?
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.user = PFUser.currentUser()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        let username: NSString = self.user!.username!
        print (username.description)
        let predicate = NSPredicate(format: "user1 = %@ OR user2 = %@", username, username)
        let query = PFQuery(className: "Conversation", predicate: predicate)
        query.findObjectsInBackgroundWithBlock { (objects: [PFObject]?, error: NSError?) -> Void in
            if error != nil {
                NSLog(error!.description)
            } else {
                var unsortedConversations: [PFObject] = []
                
                for object in objects!{
                    unsortedConversations.append(object)
                }
                // sort conversations in increasing time order
                self.conversations = unsortedConversations.sort( { ($0.updatedAt!).compare($1.updatedAt!) == NSComparisonResult.OrderedAscending } )
                self.conversationTable.reloadData()
            }
        }
        
        
        
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.conversations.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell") as UITableViewCell!
        //Setup Label with dynamic cell sizing and label lines
        let convo = conversations[indexPath.row]
        var otherUser = ""
        if convo["user1"] as! String == self.user!.username! {
            otherUser = convo["user2"] as! String
        } else {
            otherUser = convo["user1"] as! String
        }
        let userNameLabel = cell.contentView.viewWithTag(1) as! UILabel
        userNameLabel.text = otherUser
        conversationTable.estimatedRowHeight = 44.0
        conversationTable.rowHeight = UITableViewAutomaticDimension
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // When cell is clicked, set correct variable values and transistion to next view
        self.currentConversation = conversations[indexPath.row]
        performSegueWithIdentifier("conversationPress", sender:nil)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Send variable information to next class
        if segue.identifier == "conversationPress" {
            let destination = segue.destinationViewController as! ConversationViewController
            destination.conversation = self.currentConversation
        } else if segue.identifier == "newConversation" {
            let destination = segue.destinationViewController as! NewConversationViewController
            destination.conversations = self.conversations
        }
    }
}