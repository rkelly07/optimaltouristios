//
//  NewConversationViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 3/4/16.
//  Copyright © 2016 Ryan Kelly. All rights reserved.
//

import Foundation
import Parse


class NewConversationViewController: UIViewController {
    
    @IBOutlet weak var friendsTable: UITableView!
    
    var user:PFUser!
    var friendsList: [String]!
    var conversations: [PFObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user = PFUser.currentUser()
        
        self.friendsList = self.user!["friends"] as? [String]
        
        self.friendsTable.reloadData()
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.friendsList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell") as UITableViewCell!
        let friend = self.friendsList[indexPath.row]
        let friendLabel = cell.contentView.viewWithTag(1) as! UILabel
        friendLabel.text = friend
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let currentUser = friendsList[indexPath.row]
        for conversation in self.conversations {
            if conversation["user1"] as! String == currentUser || conversation["user2"] as! String == currentUser {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "You already have a conversation with this user", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                return
            }
        }
        let newConversation = PFObject(className: "Conversation")
        
        newConversation["user1"] = self.user!.username!
        newConversation["user2"] = currentUser
        
        newConversation.saveInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
            if error == nil {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "New conversation created", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                self.performSegueWithIdentifier("backToConversations", sender: nil)
            }
        }

    }

}

