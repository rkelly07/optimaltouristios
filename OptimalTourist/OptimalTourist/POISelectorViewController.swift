//
//  POISelectorViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 1/11/16.
//  Copyright © 2016 Ryan Kelly. All rights reserved.
//

import Foundation


var singaporePOIS = ["National Orchid Garden","Gardens By The Bay","Singapore Botanic Gardens","Singapore Zoo","Cloud Forest","Marina Bay","Singapore Flyer","Waterfront Promenade","MacRitchie Nature Trail","Buddha Tooth Relic Temple and Museum","Marina Bay Sands SkyPark","Changi Chapel and Museum","Flower Dome","Kranji War Memorial","Jurong Bird Park","Asian Civilisations Museum","The Southern Ridges","MacRitchie Reservoir","Clarke Quay","Orchard Road","Peranakan Museum","The Shoppes at Marina Bay Sands","Chinatown","Merlion Park","The Helix Bridge","Esplanade - Theatres on the Bay","Singapore City Gallery","National Library of Singapore","Bukit Brown Cemetary","East Coast Park","National Museum of Singapore","Memories at the Old Factory","Sungei Buloh Wetland Reserve","Singapore River","Kong Meng San Phor Kark See Temple","Marina Barrage","ION Orchard","Takashimaya Singapore","Fort Canning Park","Arab Street","Emerald Hill","The Intan","Chinatown Heritage Center","Raffles Hotel Arcade","ArtScience Museum at Marina Bay Sands","Chinese and Japanese Gardens","Lau Pa Sat Festival Pavilion","The Pinnacle @ Duxton","Sri Mariammam Temple","NUS Baba House"]

var bostonPOIS = ["Museum of Fine Arts","Fenway Park","Freedom Trail","Boston Children's Museum","Massachusetts State House","Granary Burying Ground","Back Bay","Trinity Church","Castle Island","Mapparium","Boston Harbor Islands National Recreation Area","Arnold Arboretum","Prudential Center","TD Garden","Waterfront","Old State House","USS Constitution","Newbury Street","New England Holocaust Memorial","Boston's Best Cruises","Isabella Stewart Gardner Museum","George's Island","Beacon Hill","Opera House","New England Aquarium Whale Watch","Old North Church","National Park Service Visitor Center","The Printing Office of Edes & Gill","Boston Public Library","Copley Square","John F. Kennedy Presidential Museum & Library","Improv Asylum","Charles River Esplanade","Museum of Science","Skywalk Observatory","Boston Tea Party Ships & Museums","Faneuil Hall Historic Site","USS Constitution Museum","Old South Church","Charlestown Navy Yard","King's Chapel","Rose Fitzgerald Kennedy Greenway Conservancy","Boston Convention & Exhibition Center","New England Aquarium","Boston Common","Bunker Hill Monument","North End","Captain Jackson's Historic Chocolate Shop","Boston Public Garden","Symphony Hall"]

var pois: [String]?

var buttonList: [Bool]!

protocol POISelectorDelegate {
    func sendBackPOIList(var POIList: [Int])
}

class POISelectorViewContoller: UIViewController {
    
    @IBOutlet weak var requiredDestinationsLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    var city: String!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.poiTable.reloadData()
        
    }

    var requiredIndexes: [Int]!
    var requiredDestinations: Int = 0
    @IBOutlet weak var poiTable: UITableView!
    
    var delegate: POISelectorDelegate? = nil
    
    @IBAction func requiredDestinationsStepper(sender: UIStepper) {
        requiredDestinations = Int(sender.value)
        requiredDestinationsLabel.text = "\(requiredDestinations) Required destinations"
    }
    @IBAction func selectButton(sender: AnyObject) {
        self.requiredIndexes = []
        for i in 0...(buttonList.count-1){
            if buttonList[i]{
                requiredIndexes.append(i)
            }
        }
        if requiredIndexes.count != requiredDestinations{
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "The number of selected locations does match what you specified with the stepper", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        self.delegate?.sendBackPOIList(self.requiredIndexes)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelButton(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        stepper.maximumValue = 1
        stepper.minimumValue = 0
        self.requiredIndexes = []
        buttonList = []
        
        if city == "Singapore"{
            pois = singaporePOIS
        } else {
            pois = bostonPOIS
        }
        for _ in 0...50 {
            buttonList.append(false)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return pois!.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell")! as UITableViewCell
        //Setup Label with dynamic cell sizing and label lines
        let poiLabel = cell.contentView.viewWithTag(1) as! UILabel
        
        let poiSwitch = cell.contentView.viewWithTag(2) as! customSwitch
        poiSwitch.row = indexPath.row
        
        poiLabel.text = pois![indexPath.row]
        poiTable.estimatedRowHeight = 88.0
        poiTable.rowHeight = UITableViewAutomaticDimension
        cell.selectionStyle = .None
        return cell
    }

}

class poiCell: UITableViewCell {
    
    
    @IBAction func switchChanged(sender: AnyObject) {
        buttonList[sender.row] = !buttonList[sender.row]
    }
    
}