//
//  PlannerMenuViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/29/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation

class PlannerMenuViewController:MenuViewController {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0: // Trip Planner - current view
            self.performSegueWithIdentifier("plannerPress", sender: nil)
            break
        case 1: // Maps
            if checkIfTour(){
                let storyboard2 = UIStoryboard(name: "Maps", bundle: nil)
                let vc2 = storyboard2.instantiateViewControllerWithIdentifier("maps")
                presentViewController(vc2, animated: true, completion: nil)
                break
            } else {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "You must plan a trip first or Turn on Location Settings!", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                break
            }

        case 2: //Settings
            let storyboard3 = UIStoryboard(name: "Settings", bundle: nil)
            let vc3 = storyboard3.instantiateViewControllerWithIdentifier("settings")
            presentViewController(vc3, animated: true, completion: nil)
            break
        case 3: // Messaging
            let storyboard4 = UIStoryboard(name: "Messaging", bundle: nil)
            let vc4 = storyboard4.instantiateViewControllerWithIdentifier("messaging")
            presentViewController(vc4, animated: true, completion: nil)
            break
            
        default: // Error
            NSLog("error; invalid row tapped")
            break
        }
    }
}