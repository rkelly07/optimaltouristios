//
//  PreferencesViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/9/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import UIKit
import Foundation


class PreferencesViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, POISelectorDelegate{
    
    
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var startField: UITextField!
    @IBOutlet weak var endField: UITextField!
    @IBOutlet weak var culturalSwitch: UISwitch!
    @IBOutlet weak var landmarksSwitch: UISwitch!
    @IBOutlet weak var museumsSwitch: UISwitch!
    @IBOutlet weak var outdoorsSwitch: UISwitch!
    @IBOutlet weak var theatresSwitch: UISwitch!
    @IBOutlet weak var zoosSwitch: UISwitch!
    @IBOutlet weak var otherSwitch: UISwitch!
    
    
    var cityList = ["","Boston"]//, "New York City", "Singapore", "Paris", "Milan"]
    var dateFormatter = NSDateFormatter()
    var startTimePicker : UIDatePicker!
    var endTimePicker : UIDatePicker!
    var incomingString:String!
    var incomingInt:Int!
    var neccessaryPOIS:[String] = []
    var startIndex = 12
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityField.text = incomingString
        
        
        dateFormatter.dateFormat = "HH:mm"
        
        //Set up picker view for city
        let cityPicker = UIPickerView()
        cityPicker.delegate = self
        cityPicker.dataSource = self
        if (incomingInt != nil){
           cityPicker.selectRow(incomingInt, inComponent: 0, animated: false)
        }
        self.cityField.inputView = cityPicker
        cityPicker.backgroundColor = .whiteColor()
        
        //Set up datepicker for start time
        startTimePicker = UIDatePicker()
        startTimePicker.datePickerMode = UIDatePickerMode.Time
        startField.inputView = startTimePicker
        startTimePicker.backgroundColor = .whiteColor()
        
        startTimePicker.addTarget(self, action: Selector("starthandleDatePicker"), forControlEvents: UIControlEvents.ValueChanged)
        startField.addTarget(self, action: Selector("starthandleDatePicker"), forControlEvents: UIControlEvents.EditingDidBegin)
        
        //Set up datepicker for end time
        endTimePicker = UIDatePicker()
        endTimePicker.datePickerMode = UIDatePickerMode.Time
        endField.inputView = endTimePicker
        endTimePicker.backgroundColor = .whiteColor()
        
        endTimePicker.addTarget(self, action: Selector("endhandleDatePicker"), forControlEvents: UIControlEvents.ValueChanged)
        endField.addTarget(self, action: Selector("endhandleDatePicker"), forControlEvents: UIControlEvents.EditingDidBegin)
        
        //Create toolbar for pickerview and datepickers
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("doneButtonPressed"))
        toolBar.setItems([doneButton], animated: false)
        toolBar.userInteractionEnabled = true
        cityField.inputAccessoryView = toolBar
        startField.inputAccessoryView = toolBar
        endField.inputAccessoryView = toolBar
    }
    @IBAction func planTrip(sender: AnyObject) {
        let poiVC = self.storyboard?.instantiateViewControllerWithIdentifier("poi") as! POISelectorViewContoller
        poiVC.delegate = self
        poiVC.city = cityField.text
        
        let timeBudget = getTimeBudget(startField.text!,end: endField.text!)
        // Field Validation
        if startField.text!.characters.count == 0 {
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "You must enter a start time!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        if endField.text!.characters.count == 0 {
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "You must enter an end time!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        if !checkEndTimeAfterStartTime(startField.text!, end: endField.text!) ||  timeBudget == 0{
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "Your start time must be before your end time!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        if cityField.text == "Singapore" || cityField.text == "Boston"{
            self.presentViewController(poiVC, animated: true, completion: nil)
        } else {
            performSegueWithIdentifier("plan", sender:nil)
        }
        
    }
    // Functions for the configuration of the date and time pickers
    func starthandleDatePicker(){
        startField.text =  dateFormatter.stringFromDate(startTimePicker.date)
    }
    func endhandleDatePicker(){
        endField.text =  dateFormatter.stringFromDate(endTimePicker.date)
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int  {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cityList.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cityList[row]
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cityField.text = cityList[row]
    }
    // When the done button is pressed, hide the picker or date view
    func doneButtonPressed()
    {
        self.cityField.resignFirstResponder()
        //self.startField.text = dateFormatter.stringFromDate(startTimePicker.date)
        self.startField.resignFirstResponder()
        //self.endField.text = dateFormatter.stringFromDate(endTimePicker.date)
        self.endField.resignFirstResponder()
    }
    // Get weighting from switch to be used in algorithm
    func getWeightingFromSwitch(sw: UISwitch) -> Double{
        if sw.on{
            return 1.0
        }
        else{
            return 0.5
        }
    }
    // Parse the time into minutes
    func getTimeBudget(start: String, end: String) -> Double{
        if startField.text!.characters.count == 0 || endField.text!.characters.count == 0{
            return 0
        }
        var splitStart = start.characters.split {$0 == ":"}.map { String($0) }
        var splitEnd = end.characters.split {$0 == ":"}.map { String($0) }
        let startHR = Int(splitStart[0])
        let startMIN = Int(splitStart[1])
        let endHR = Int(splitEnd[0])
        let endMIN = Int(splitEnd[1])
        
        return Double((endHR!*60+endMIN!)-(startHR!*60+startMIN!))
    }
    // Time Validation
    func checkEndTimeAfterStartTime(start: String, end: String) -> Bool {
        var splitStart = start.characters.split {$0 == ":"}.map { String($0) }
        var splitEnd = end.characters.split {$0 == ":"}.map { String($0) }
        let startHR = Int(splitStart[0])
        let startMIN = Int(splitStart[1])
        let endHR = Int(splitEnd[0])
        let endMIN = Int(splitEnd[1])
        
        return (startHR!*60+startMIN! < endHR!*60+endMIN!)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "plan" {
            let destination = segue.destinationViewController as! ProgressViewController
            //Send preferences to progress view controller
            destination.incomingCity = cityField.text
            destination.incomingTimeBudget = getTimeBudget(startField.text!,end: endField.text!)
            destination.incomingCultural = getWeightingFromSwitch(culturalSwitch)
            destination.incomingLandmarks = getWeightingFromSwitch(landmarksSwitch)
            destination.incomingMuseums = getWeightingFromSwitch(museumsSwitch)
            destination.incomingOutdoors = getWeightingFromSwitch(outdoorsSwitch)
            destination.incomingTheaters = getWeightingFromSwitch(theatresSwitch)
            destination.incomingZoos = getWeightingFromSwitch(zoosSwitch)
            destination.incomingOther = getWeightingFromSwitch(otherSwitch)
            destination.incomingStartIndex = self.startIndex
        }
        
    }
    
    func sendBackPOIList(POIList: [Int]){
        if (POIList.count == 0 ){
            self.startIndex = 12
        } else if (POIList.count == 1) {
            self.startIndex = POIList[0]
        }
        performSegueWithIdentifier("plan", sender:nil)
    }
    
    
}

