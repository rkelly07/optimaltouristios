//
//  ProgressViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/11/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit

class ProgressViewController: UIViewController {
    
    var incomingCity:String!
    var incomingTimeBudget:Double!
    var incomingCultural:Double!
    var incomingLandmarks:Double!
    var incomingMuseums:Double!
    var incomingOutdoors:Double!
    var incomingTheaters:Double!
    var incomingZoos:Double!
    var incomingOther:Double!
    var incomingStartIndex:Int!
    
    var itinerary: [[String!]] = [[],[],[]]
    var locations: [[String!]] = [[],[],[]]
    var city: String!
    var steptime: [[Double]] = [[],[],[]]
    var completed: [Bool] = []
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        for x in 0...2 {
            if x == 0 {
                httpRequest(incomingStartIndex, functionCall: x)
            } else {
                httpRequest((incomingStartIndex * random()) % 50, functionCall: x)
            }
        }
    }
    
    func httpRequest(startIndex: Int, functionCall: Int) {
        
        let urlpath : String = "http://128.30.27.106:8085/trip?"
        //var err: NSError?
        // String that will be used for HTTP Post request data
        let queryString = "city=\(incomingCity)&time_budget=\(incomingTimeBudget)&cultural=\(incomingCultural)&landmarks=\(incomingLandmarks)&museums=\(incomingMuseums)&outdoors=\(incomingOutdoors)&theaters=\(incomingTheaters)&zoos=\(incomingZoos)&other=\(incomingOther)&startIndex=\(startIndex)"
        // Encode the queryString to the proper data format
        let http_body = queryString.dataUsingEncoding(NSUTF8StringEncoding)
        // Create URL from the urlpath string
        // urlfinal = NSURL(string: urlpath+queryString)
        // Create request and session using the URL
        let request = NSMutableURLRequest(URL: NSURL(string: urlpath)!)
        let session = NSURLSession.sharedSession()
        // Configure the HTTP request to be of type POST and set the required conditions
        request.HTTPMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("128.30.27.106:8085", forHTTPHeaderField: "Host")
        request.HTTPBody = http_body!
        request.timeoutInterval = 300
        print(http_body)
        // Create a task to communicate with the server
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if (error != nil){
                print(error!.localizedDescription)
                print("error")
            }
            else{
                //println("Response: \(response)")
                //Encode the response data to string
                // strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                //println("Body: \(strData)")
                //let err: NSError?
                //println("test")
                //println(data)
                
                //JSON conditional parsing
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? [String:AnyObject],
                    start = json?["start"] as? [String:AnyObject],
                    startpoi = start?["poi"] as? NSDictionary,
                    startname = startpoi?["name"] as? String,
                    cityname = startpoi?["city"] as? String,
                    start_stay_duration = start?["stay_duration"] as? Double,
                    steps = json?["steps"] as? [AnyObject],
                    end = json?["end"] as? [String:AnyObject],
                    endpoi = end?["poi"] as? NSDictionary,
                    endname = endpoi?["name"] as? String,
                    end_travel_type = end?["travel_type"] as? String,
                    end_travel_time = end?["travel_duration"] as? Double
                    self.city = cityname
                    self.steptime[functionCall].append(start_stay_duration!)
                    //self.itinerary.append("1.Start from \(startname), stay for \(self.formatTime(start_stay_duration!))")
                    self.itinerary[functionCall].append("Start from \(startname!), stay for ")
                    self.locations[functionCall].append(startname)
                    var x = 2
                    //println(startname)
                    // For each stop on the trip, add a new entry to the itinerary
                    for step in steps!{
                        if let poi = step["poi"] as? NSDictionary,
                            name = poi["name"] as? String,
                            stay_duration = step["stay_duration"] as? Double,
                            travel_type = step["travel_type"] as? String,
                            travel_time = step["travel_duration"] as? Double
                        {
                            self.steptime[functionCall].append(stay_duration)
                            if travel_type == "TAXI"{
                                //self.itinerary.append("\(x).Take a taxi to \(name) (\(self.formatTime(travel_time))), stay for \(self.formatTime(stay_duration))")
                                self.itinerary[functionCall].append("Take a taxi to \(name) (\(self.formatTime(travel_time))), stay for ")
                                self.locations[functionCall].append(name)
                            }
                            else if travel_type == "WALK"{
                                //self.itinerary.append("\(x).Walk to \(name) (\(self.formatTime(travel_time))), stay for \(self.formatTime(stay_duration))")
                                self.itinerary[functionCall].append("Walk to \(name) (\(self.formatTime(travel_time))), stay for ")
                                self.locations[functionCall].append(name)
                            }
                            x += 1
                        }
                    }
                    self.steptime[functionCall].append(end_travel_time!)
                    if end_travel_type == "TAXI"{
                        //self.itinerary.append("\(x).Return to \(endname) by taxi (\(self.formatTime(end_travel_time!)))")
                        self.itinerary[functionCall].append("Return to \(endname!) by taxi ")
                        self.locations[functionCall].append(endname)
                    }
                    else if end_travel_type == "WALK"{
                        //self.itinerary.append("\(x).Return to \(endname) by walking (\(self.formatTime(end_travel_time!)))")
                        self.itinerary[functionCall].append("Return to \(endname!) by walking ")
                        self.locations[functionCall].append(endname)
                    }
                    
                }catch let error as NSError {
                    print(error.localizedDescription)
                }catch{
                }
            }
            self.completed.append(true)
            if self.completed.count == 3 {
                self.performSegueWithIdentifier("finishedPlanning", sender: nil)
            }
            
        })
        task.resume()

    }
    func formatTime(time: Double) -> String{
        if time >= 60.0 {
            let hoursTime = time/60.0
            let mins = Int(roundToFives(getDecimal(hoursTime)*60.0))
            let hours = Int(floor(hoursTime))
            return "\(hours) hours and \(mins) minutes"
        }
        let roundedMinute = Int(round(time))
        return "\(roundedMinute) minutes"
    }
    func roundToFives(x : Double) -> Int {
        return 5 * Int(round(x / 5.0))
    }
    func getDecimal(x: Double) -> Double {
        let intPart = floor(x)
        return x - intPart
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Prepare to send the itinerary to the next class when the segue is performed
        if segue.identifier == "finishedPlanning" {
            let destination = segue.destinationViewController as! ItineraryViewController
                destination.incomingItinerary = self.itinerary
                destination.incomingLocations = self.locations
                destination.incomingCity = self.city
                destination.incomingTimes = self.steptime
            }
    }
}