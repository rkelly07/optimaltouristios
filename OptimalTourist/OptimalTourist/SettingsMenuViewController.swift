//
//  SettingsMenuViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 7/10/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit

class SettingsMenuViewController: MenuViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row {
        case 0: // Trip Planner 
            let storyboard1 = UIStoryboard(name: "Planner", bundle: nil)
            let vc1 = storyboard1.instantiateViewControllerWithIdentifier("city") 
            presentViewController(vc1, animated: true, completion: nil)
            break
        case 1: // Maps
            if checkIfTour(){
                let storyboard2 = UIStoryboard(name: "Maps", bundle: nil)
                let vc2 = storyboard2.instantiateViewControllerWithIdentifier("maps")
                presentViewController(vc2, animated: true, completion: nil)
                break
            } else {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "You must plan a trip first or Turn on Location Settings!", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
                break
            }

        case 2: // Settings
            self.performSegueWithIdentifier("settingsPress", sender: nil)
            break
        case 3: // Messaging
            let storyboard3 = UIStoryboard(name: "Messaging", bundle: nil)
            let vc3 = storyboard3.instantiateViewControllerWithIdentifier("messaging")
            presentViewController(vc3, animated: true, completion: nil)
        default: // Error
            NSLog("error; invalid row tapped")
            break
        }
    }
}