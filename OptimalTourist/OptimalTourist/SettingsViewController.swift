//
//  SettingsViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 7/10/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit
import Parse


class SettingsViewController: UIViewController{
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var usernameField: UITextField!
    
    var user: PFUser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user = PFUser.currentUser()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    @IBAction func addButton(sender: AnyObject) {
        if self.usernameField.text! == "" {
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "Please enter a username", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        var currentFriends = self.user!["friends"] as! [String]!
        
        if currentFriends.contains(self.usernameField.text!){
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "This User is already your friend!", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            currentFriends.append(self.usernameField.text!)
            self.user!["friends"] = currentFriends
            self.user!.saveInBackground()
        }
    }
}