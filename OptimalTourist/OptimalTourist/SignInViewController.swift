//
//  SignInViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/22/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit
import Parse

class SignInViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var userText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var successfulLogin:Int?
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userText.delegate = self
        passwordText.delegate = self
        activityIndicator.hidden = true
        
    }
    
    override func viewDidAppear(animated: Bool) {
        /*super.viewDidAppear(animated)
        if (defaults.boolForKey("loggedIn") == true){
            var cityStoryboard = UIStoryboard(name: "Planner", bundle: nil)
            var cityVC = cityStoryboard.instantiateViewControllerWithIdentifier("city") as! UIViewController
            self.presentViewController(cityVC, animated: false, completion: nil)
        }*/
    }


    @IBAction func signIn(sender: AnyObject) {
        if !(userText.text!.isEmpty) || !(passwordText.text!.isEmpty) {
            // Start login
            activityIndicator.startAnimating()
            activityIndicator.hidden = false
            PFUser.logInWithUsernameInBackground(self.userText.text!, password: self.passwordText.text!, block: { (success, error) in
                print(self.passwordText.text!)
                if (success != nil) {
                    //let alert = UIAlertView(title: "Success", message: "Logged In", delegate: self, cancelButtonTitle: "OK")
                    //alert.show()
                    // Stop the spinner
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.hidden = true
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        let cityStoryboard = UIStoryboard(name: "Planner", bundle: nil)
                        let cityVC = cityStoryboard.instantiateViewControllerWithIdentifier("city")
                        self.presentViewController(cityVC, animated: false, completion: nil)
                    })
                    
                } else {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.hidden = true
                    let alertController = UIAlertController(title: "OptimalTourist", message:
                        "Please enter a username and password", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            })
        }
    }
            /*// Display error message or go to events
            if success == nil {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "Invalid login credentials", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            } else {
                // Display cityvcd
                let cityStoryboard = UIStoryboard(name: "Planner", bundle: nil)
                let cityVC = cityStoryboard.instantiateViewControllerWithIdentifier("city")
                self.presentViewController(cityVC, animated: false, completion: nil)
            }
        } else {
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "Please enter a username and password", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }*/
        //Below this is the old login code with the mysql server
        /*if !(userText.text!.isEmpty) || !(passwordText.text!.isEmpty) {
            activityIndicator.startAnimating()
            activityIndicator.hidden = false
            let urlpath : String = "http://128.30.27.129/login/signin.php"
            // String that will be used for HTTP Post request data
            let queryString = "username=\(userText.text!)&password=\(passwordText.text!)"
            // Encode the queryString to the proper data format
            print(queryString)
            let http_body = queryString.dataUsingEncoding(NSUTF8StringEncoding)
            // Create URL from the urlpath string
            // = NSURL(string: urlpath+queryString)
            // Create request and session using the URL
            let request = NSMutableURLRequest(URL: NSURL(string: urlpath)!)
            let session = NSURLSession.sharedSession()
            // Configure the HTTP request to be of type POST and set the required conditions
            request.HTTPMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.addValue("128.30.24.150", forHTTPHeaderField: "Host")
            request.HTTPBody = http_body
            // Create a task to communicate with the server
            let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                if (error != nil){
                    print(error!.localizedDescription)
                    print("error")
                }
                else{
                    //println("Response: \(response)")
                    //Encode the response data to string
                    let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    //println("Body: \(strData)")
                    //println("test")
                    //println(data)
                    print(strData)
                    //JSON conditional parsing
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary, success = json["LoginSuccessful"] as! Int!
                        // Display error message or go to events
                            print(success)
                            self.successfulLogin = success
                            
                        } catch let error as NSError{
                            print(error.localizedDescription)
                            
                    }
                    
                    }
            })
            task.resume()
            while (successfulLogin == nil){
                continue
            }
            activityIndicator.stopAnimating()
            activityIndicator.hidden = true
            if successfulLogin == 0 {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "Invalid login credentials", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            } else if successfulLogin == 1 {
                
                defaults.setObject(userText.text, forKey: "userNameKey")
                defaults.setBool(true, forKey: "loggedIn")
                defaults.synchronize()
                let cityStoryboard = UIStoryboard(name: "Planner", bundle: nil)
                let cityVC = cityStoryboard.instantiateViewControllerWithIdentifier("city")
                self.presentViewController(cityVC, animated: false, completion: nil)
            }
        }
        else {
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "Please enter a username and password", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }*/
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Allows text field to be exited after entering text
        textField.resignFirstResponder()
        return true
    }
}