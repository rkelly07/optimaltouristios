//
//  SignUpViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/22/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit
import Parse

class SignUpViewController: UIViewController, UITextFieldDelegate{
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var firstnameField: UITextField!
    @IBOutlet weak var lastnameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var reenterField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var successfulSignup: Int?
    var signupError: String?
    var originalConstraint: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameField.delegate = self
        firstnameField.delegate = self
        lastnameField.delegate = self
        emailField.delegate = self
        passwordField.delegate = self
        reenterField.delegate = self
        activityIndicator.hidden = true
    }
    
    @IBAction func signUp(sender: AnyObject) {
        
        let user = PFUser()
        if !(usernameField.text!.isEmpty) || !(firstnameField.text!.isEmpty) || !(lastnameField.text!.isEmpty) || !(emailField.text!.isEmpty) || !(passwordField.text!.isEmpty) || !(reenterField.text!.isEmpty) {
            if (passwordField.text == reenterField.text) {
                activityIndicator.startAnimating()
                activityIndicator.hidden = false
                user.username = usernameField.text
                user.password = passwordField.text
                user.email = emailField.text
                user["firstname"] = firstnameField.text
                user["lastname"] = lastnameField.text
                user["friends"] = []
                user["itineraries"] = []
                user["currentDestination"] = ""
                user["latitude"] = ""
                user["longitude"] = ""
                user["currentCity"] = ""
        
                user.signUpInBackgroundWithBlock { (success, error) in
                    if success {
                        self.usernameField.text = ""
                        self.firstnameField.text = ""
                        self.lastnameField.text = ""
                        self.emailField.text = ""
                        self.passwordField.text = ""
                        self.reenterField.text = ""
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.hidden = true
                        let alertController = UIAlertController(title: "OptimalTourist", message:
                            "Success!", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.performSegueWithIdentifier("returnToLogin", sender: nil)
                        })
                    } else {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.hidden = true
                        print(error)
                        let alertController = UIAlertController(title: "OptimalTourist", message:
                            "Error", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                }
            } else {
                let alertController = UIAlertController(title: "OptimalTourist", message:
                    "Your passwords must match!", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            
        } else {
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "Please fill out all of the required fields", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
        
        // Below is the code for the old mysqldatbase signup form
        /*if !(usernameField.text!.isEmpty) || !(firstnameField.text!.isEmpty) || !(lastnameField.text!.isEmpty) || !(emailField.text!.isEmpty) || !(passwordField.text!.isEmpty) || !(reenterField.text!.isEmpty) {
            if (passwordField.text == reenterField.text) {
                let urlpath : String = "http://128.30.24.150/login/signup.php"
                //var err: NSError?
                // String that will be used for HTTP Post request data
                let queryString = "username=\(usernameField.text)&firstname=\(firstnameField.text)&lastname=\(lastnameField.text)&email=\(emailField.text)&password=\(passwordField.text)"
                // Encode the queryString to the proper data format
                print(queryString)
                let http_body = queryString.dataUsingEncoding(NSUTF8StringEncoding)
                // Create URL from the urlpath string
                //var urlfinal = NSURL(string: urlpath+queryString)
                // Create request and session using the URL
                let request = NSMutableURLRequest(URL: NSURL(string: urlpath)!)
                let session = NSURLSession.sharedSession()
                // Configure the HTTP request to be of type POST and set the required conditions
                request.HTTPMethod = "POST"
                request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                request.addValue("128.30.24.150", forHTTPHeaderField: "Host")
                request.HTTPBody = http_body
                // Create a task to communicate with the server
                let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
                    if (error != nil){
                        print(error!.localizedDescription)
                        print("error")
                    }
                    else{
                        //Encode the response data to string
                        let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                        //var err: NSError?
                        print(strData)
                        //JSON conditional parsing
                        do {
                            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary, success = json["SignupSuccessful"] as? Int, message = json["Error"] as? String
                                // Display error message or go to events
                                print(success)
                                self.signupError = message
                                self.successfulSignup = success
                            } catch let error as NSError {
                                print(error.localizedDescription)
                            }
                        }
                })
                task.resume()
                while (successfulSignup == nil){
                    continue
                }
                if successfulSignup == 0 {
                    let alertController = UIAlertController(title: "OptimalTourist", message:
                        self.signupError, preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                } else if successfulSignup == 1 {
                    usernameField.text = ""
                    firstnameField.text = ""
                    lastnameField.text = ""
                    emailField.text = ""
                    passwordField.text = ""
                    reenterField.text = ""
                    let alertController = UIAlertController(title: "OptimalTourist", message:
                        "Success!", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                    self.performSegueWithIdentifier("returnToLogin", sender: nil)
                }
            
            }
                
        }
        else {
            let alertController = UIAlertController(title: "OptimalTourist", message:
                "Please fill out all of the required fields", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }*/
        
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Allows text field to be exited after entering text
        textField.resignFirstResponder()
        return true
    }

}