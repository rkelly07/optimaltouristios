//
//  SocialViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 1/8/16.
//  Copyright © 2016 Ryan Kelly. All rights reserved.
//

import Foundation
import UIKit
import Parse
import MapKit
import CoreLocation

protocol FriendDisplayDelegate {
    func sendBackFriendList(var friendList: [ColorPointAnnotation])
}

class SocialViewController: UIViewController, CLLocationManagerDelegate {
    
    var user: PFUser?
    var friendslist: [String]?
    var friendsObjects: [PFObject]?
    var closeFriends: [(PFObject,CLLocationCoordinate2D)] = []
    var destinationFriends: [(PFObject,CLLocationCoordinate2D)] = []
    var chosenFriends: [ColorPointAnnotation] = []
    var delegate: FriendDisplayDelegate? = nil
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    
    @IBOutlet weak var nearby: UIButton!
    @IBOutlet weak var destination: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        self.chosenFriends = []
        self.user = PFUser.currentUser()
        self.friendslist = self.user!["friends"] as? [String]
        print(self.user!)
        let query = PFQuery(className: "_User")
        query.whereKey("username", containedIn: self.friendslist!)
        query.findObjectsInBackgroundWithBlock { (object, error) -> Void in
            if error == nil {
                self.friendsObjects = object
                self.nearby.hidden = false
                self.destination.hidden = false
            }
        }
        
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         currentLocation = manager.location!
        
    }
    
    @IBAction func nearbyButton(sender: AnyObject) {
        for friend in self.friendsObjects! {
            let lat = friend["latitude"] as! String
            let lon = friend["longitude"] as! String
            if (lat == "") || (lon == ""){
                continue
            }
            let latdouble = Double(lat)
            
            let londouble = Double(lon)
            let friendCoordinates = CLLocation(latitude: latdouble!, longitude: londouble!)
            let friendDist = friendCoordinates.distanceFromLocation(self.currentLocation!)
            if friendDist <= 20000 {
                self.closeFriends.append((friend, friendCoordinates.coordinate))
            }
        }
        friendToAnnotation(self.closeFriends)
        self.delegate?.sendBackFriendList(self.chosenFriends)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func destinationButton(sender: AnyObject) {
        let currentDest = self.user!["currentDestination"] as! String
        for friend in self.friendsObjects! {
            let dest = friend["currentDestination"] as! String
            if dest == "Overview" {
                continue
            }
            if currentDest != dest{
                continue
            }
            let lat = friend["latitude"] as! String
            let lon = friend["longitude"] as! String
            if (lat == "") || (lon == ""){
                continue
            }
            let latdouble = Double(lat)
            
            let londouble = Double(lon)
            let friendCoordinates = CLLocation(latitude: latdouble!, longitude: londouble!)
            self.destinationFriends.append((friend, friendCoordinates.coordinate))
        
        }
        friendToAnnotation(self.destinationFriends)
        self.delegate?.sendBackFriendList(self.chosenFriends)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func friendToAnnotation(friends: [(PFObject, CLLocationCoordinate2D)]) {
        for friend in friends {
            let friendToAdd = ColorPointAnnotation(pinColor: UIColor.blueColor())
            friendToAdd.title = "\(friend.0["firstname"]) \(friend.0["lastname"])"
            friendToAdd.coordinate = friend.1
            friendToAdd.subtitle = "friend"
            self.chosenFriends.append(friendToAdd)
        }
    }
    @IBAction func doneButton(sender: AnyObject) {
        self.delegate?.sendBackFriendList(self.chosenFriends)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
class ColorPointAnnotation: MKPointAnnotation {
    var pinColor: UIColor
    
    init(pinColor: UIColor) {
        self.pinColor = pinColor
        super.init()
    }
}