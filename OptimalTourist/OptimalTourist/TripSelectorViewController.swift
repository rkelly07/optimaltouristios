//
//  TripSelectorViewController.swift
//  OptimalTourist
//
//  Created by Ryan Kelly on 6/25/15.
//  Copyright (c) 2015 Ryan Kelly. All rights reserved.
//

import Foundation

class TripSelectorViewController: UIViewController {
    @IBOutlet weak var tripSelectorTable: UITableView!
    let trips: [String] = ["Itinerary 1","Itinerary 2", "Itinerary 3"]
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        
        
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    return self.trips.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell") as UITableViewCell!
        //Setup Label with dynamic cell sizing and label lines
        let itineraryLabel = cell.contentView.viewWithTag(1) as! UILabel
        itineraryLabel.text = trips[indexPath.row]
        tripSelectorTable.estimatedRowHeight = 44.0
        tripSelectorTable.rowHeight = UITableViewAutomaticDimension
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // When cell is clicked, set correct variable values and transistion to next view
        defaults.setInteger(indexPath.row, forKey: "currentTrip")
        performSegueWithIdentifier("backToMaps", sender:nil)
    }
}